# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.2] - 2023-06-16

### Added

- icons: filter

## [1.4.1] - 2020-05-29

### Added

- icons: bluetooth

## [1.4.0] - 2020-03-20

### Added

- default icons synonyms
- table filter
- extended icons synonyms
- icons: copy-clipboard, eye, eye-closed, fullscreen, windowed, incognito

### Changed

- added default UIkit icons to list

### Fixed

- main entry point
- dependencies version bump
- updated domain and git url

## [1.3.0] - 2019-08-29

### Added

- various charts icons
- dashboard icon
- import icons from figma

## [1.2.0] - 2019-07-25

### Added

- npm and gitlab logos
- battery icons
- folder and folders icon
- terminal icon

## [1.0.0] - 2019-07-01

### Added

- created package

[unreleased]: https://gitlab.com/insensaty/uikit-icons-extended/tree/master
[1.0.0]: https://gitlab.com/insensaty/uikit-icons-extended/-/tags/1.0.0
