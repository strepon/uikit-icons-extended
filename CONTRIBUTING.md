# Contributing

Create icon and send merge request.

## Creating icon

I'm using [figma](http://figma.com), but any svg editor will work.

-   **size**: 20x20 or 20x24
-   **viewBox**: "0 0 20 20" or "0 0 20 24" must be present
-   **stroke & fill**: #000000
-   **angles**: 90° or 45° (exceptions allowed, eg: logos)

## Adding alias/synonym

Edit `src/icons-details.json` and submit merge request.
