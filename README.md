# UIkit icons extended

Library extending UIkit basic [icons set](https://getuikit.com/docs/icon#library).

[![pipeline status](https://gitlab.com/hajnyon/uikit-icons-extended/badges/master/pipeline.svg)](https://gitlab.com/hajnyon/uikit-icons-extended/pipelines)
[![npm version](https://img.shields.io/npm/v/uikit-icons-extended.svg)](https://www.npmjs.com/package/uikit-icons-extended)
[![extended icons](https://img.shields.io/endpoint?url=https%3A%2F%2Fhajnyon.gitlab.io%2Fuikit-icons-extended%2Fbadge.json)](https://hajnyon.gitlab.io/uikit-icons-extended/#icons)
[![jsDelivr hits](https://data.jsdelivr.com/v1/package/npm/uikit-icons-extended/badge)](https://www.jsdelivr.com/package/npm/uikit-icons-extended)

Links to see:

-   [extended icons list](https://hajnyon.gitlab.io/uikit-icons-extended)
-   [changelog](https://gitlab.com/hajnyon/uikit-icons-extended/blob/master/CHANGELOG.md) for planed icons
-   [contributing](https://gitlab.com/hajnyon/uikit-icons-extended/blob/master/CONTRIBUTING.md) if you want to suggest or participate

### TBD

-   [x] Import svgs directly from figma.com
-   [x] Icons count badge
-   [x] Web: search icons (+ add aliases for better search)
-   [x] Web: add default UIkit icons for better search
-   [ ] **More icons!**

## Install

```bash
npm install uikit-icons-extended
```

OR

[download file](https://hajnyon.gitlab.io/uikit-icons-extended/uk-extended.min.js)

```html
<script src="path/to/uk-extended.min.js"></script>
```

OR

Use CDN

```html
<script src="https://cdn.jsdelivr.net/npm/uikit-icons-extended@1/dist/js/uk-extended.min.js"></script>
```

## Usage

[Icons list](https://hajnyon.gitlab.io/uikit-icons-extended)

```html
<span uk-icon="save"></span>
```

### With TypeScript

```ts
import UKExtendedIcons from 'uikit-icons-extended/dist/js/uk-extended.min';

UIkit.use(UKExtendedIcons);
```

## Development

```bash
git clone git@gitlab.com:hajnyon/uikit-icons-extended.git && cd uikit-icons-extended
npm install
npm run build
```

### Import icons from figma

-   setup `.env` file wih figma access token
-   run `npm run icons:import`
