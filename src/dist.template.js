(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define('uikiticons', factory) :
            (global.UIkitIcons = factory());
}(this, (function () {
    'use strict';

    let Icons = {
        REPLACE_ICONS
    };

    function plugin(UIkit) {

        if (plugin.installed) {
            return;
        }

        UIkit.icon.add(Icons);

    }

    if (typeof window !== 'undefined' && window.UIkit) {
        window.UIkit.use(plugin);
    }

    return plugin;

})));
