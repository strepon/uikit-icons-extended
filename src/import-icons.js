/**
 * Imports svg icons from figma file.
 */

// cSpell:ignore figma
const Figma = require('figma-api');
const fetch = require('node-fetch');
const fse = require('fs-extra');
require('dotenv').config();

const DIR = './icons';
const FILE_KEY = 'K2DORwwpJ7VlsNQ6HzXWit';

async function getIcons(count) {
    const api = new Figma.Api({
        personalAccessToken: process.env.FIGMA_TOKEN
    });

    const file = await api.getFile(FILE_KEY);
    const figmaIcons = file.document.children[0].children[1].children;

    let icons = {};
    const ids = [];
    for (const icon of figmaIcons) {
        ids.push(icon.id);
        icons[icon.id] = {
            node: icon,
            svg: ''
        };
    }

    let iconsFiles = {};
    try {
        iconsFiles = await api.getImage(FILE_KEY, { ids: ids.join(','), format: 'svg', svg_include_id: true });
    } catch (error) {
        console.error(error);
    }
    for (const id in iconsFiles.images) {
        if (iconsFiles.images.hasOwnProperty(id)) {
            try {
                const path = iconsFiles.images[id];
                const svg = await fetch(path, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'image/xml+svg; charset=utf-8'
                    }
                });
                const textSvg = await svg.text();
                icons[id].svg = textSvg;
                count.success++;
            } catch (error) {
                delete icons[id];
                count.error++;
                console.error(error);
            }
        }
    }
    return icons;
}

async function saveIcons(icons) {
    try {
        await fse.emptyDir(DIR);
    } catch (error) {
        console.error(error);
    }

    for (const icon in icons) {
        if (icons.hasOwnProperty(icon)) {
            try {
                await fse.outputFile(`${DIR}/${icons[icon].node.name}.svg`, icons[icon].svg);
            } catch (error) {
                console.error(error);
            }
        }
    }
}

async function main() {
    const count = {
        success: 0,
        error: 0
    };

    const icons = await getIcons(count);
    await saveIcons(icons);

    console.log(`
    \x1b[32m${count.success} icons imported.\x1b[0m`);
    if (count.error > 0) {
        console.log(`\x1b[31m${count.error} icons failed to import.\x1b[0m`);
    }
}

main();
