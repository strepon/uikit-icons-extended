/**
 * Build html table from icons folder.
 */

const Twig = require('twig');
const fs = require('fs');
const fse = require('fs-extra');

const uikitIcons = require('uikit/dist/js/uikit-icons');

const DIR = './icons';
const DIST_DIR = './dist-doc';
const BADGE_FILE_NAME = 'badge.json';
const ICONS_DETAILS_FILE = './src/icons-details.json';

class IconsGetter {
    icons = {};
    icon = {
        add: (icons) => {
            this.icons = icons;
        }
    };
}

async function buildIndex(icons) {
    Twig.renderFile('./doc/index.twig', { icons: icons }, async (err, html) => {
        await fse.outputFile(`${DIST_DIR}/index.html`, html);
    });
}

async function buildBadge(count) {
    const badgeInfo = {
        schemaVersion: 1,
        label: 'extended icons',
        message: String(count),
        color: 'yellow'
    };
    await fse.outputJSON(`${DIST_DIR}/${BADGE_FILE_NAME}`, badgeInfo);
}

async function build() {
    try {
        const files = fs.readdirSync(DIR, { withFileTypes: true });
        const iconsDetails = await fse.readJSON(ICONS_DETAILS_FILE);

        const icons = [];

        // extended icons
        for (const file of files) {
            if (file.isFile()) {
                const name = file.name.replace('.svg', '');
                icons.push({ name: name, type: 'extended', aliases: iconsDetails[name] ? iconsDetails[name].aliases : [] });
            }
        }
        const extendedIconsCount = icons.length;
        await buildBadge(extendedIconsCount);

        // default icons
        const tmp = new IconsGetter();
        uikitIcons(tmp);
        const defaultIcons = tmp.icons;
        for (const iconKey in defaultIcons) {
            if (defaultIcons.hasOwnProperty(iconKey)) {
                icons.push({ name: iconKey, type: 'default', aliases: iconsDetails[iconKey] ? iconsDetails[iconKey].aliases : [] });
            }
        }

        await buildIndex(icons);
    } catch (error) {
        console.error(error);
    }
}

build();
